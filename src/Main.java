
class Print{
     static synchronized void printString(String str){
        try{
            char[] ch = str.toCharArray();
            for (int i =0 ; i< ch.length; i++){
                System.out.print(ch[i]);
                Thread.sleep(300);
            }
        }catch (Exception ex){
            System.out.println(ex);
        }
    }
}
class Thread1 extends Thread{
    public void run(){Print.printString("Hello KSHRD!\n");}
}
class Thread2 extends Thread{
    public void run(){Print.printString("*************************************\n");}
}
class Thread3 extends  Thread{
    public void run(){Print.printString("I will try my best to be here at HRD.\n");}
}
class Thread4 extends  Thread {
    public void run() {Print.printString("-------------------------------------\n"); }
}
class Thread5 extends Thread{
    public void run() {
        synchronized (this){
            Print.printString("........");
            notify();
        }

    }
}
public class Main {
    public static void main(String[] args) {
        Thread1 t1 = new Thread1();
        Thread2 t2 = new Thread2();
        Thread3 t3 = new Thread3();
        Thread4 t4 = new Thread4();
        Thread5 t5 = new Thread5();
        t1.start();
        try {
            t1.join();
        }catch (Exception exp){}
        t2.start();
        try{
            t2.join();
        }catch (Exception exp){}
        t3.start();
        try{
            t3.join();
        }catch (Exception exp){}
        t4.start();
        try{
            t4.join();
        }catch (Exception exp){}
        t5.start();
        synchronized (t5){
            try {
                System.out.print("Downloading");
                t5.wait();
                System.out.println("Completed 100%!");
            }catch (Exception exp){}
        }
    }
}
